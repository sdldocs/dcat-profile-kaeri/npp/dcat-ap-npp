# Resource Class Description

@prefix adms: <http://www.w3.org/ns/adms#> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix dcat: <http://www.w3.org/ns/dcat#> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix dctype: <http://purl.org/dc/dcmitype/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix odrl: <http://www.w3.org/ns/odrl/2/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix prov: <http://www.w3.org/ns/prov#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix spdx: <http://spdx.org/rdf/terms#> .
@prefix time: <http://www.w3.org/2006/time#> .
@prefix vcard: <http://www.w3.org/2006/vcard/ns#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

# the namespace of the KAERI Project
@prefix knpp: <http:/www.kaeri.re.kr/knpp/> . 
@prefix person: <http:/www.kaeri.re.kr/knpp/persons#> .
@prefix organization: <http:/www.kaeri.re.kr/knpp/organization#> .

# image type 파일의 한 example
# 이같은 image 파일은 Catalog와 Distribution에 한 instance로 등록되어야 할 것 같음
# "A-TH-15-6-5-SBLOCA-CL-CHA-0.9INCH.csv" 파일의 "Nordalization" Sheet
# csv 파일 내의 각 sheet 하나를 한 resource로 취급함
# resource-opr1000-nodalization
knpp:opr1000-nodes-image   
    a dcat:Resource ;
    adms:status <http://purl.org/adms/status/Completed> ;   # status is one of “completed”, “deprecated”, “under development”, and “withdrawn” from http://purl.org/adms/status/
    adms:versionNotes “the first version of nordalization image for NPP with OPR 1000 nuclear reactor type”@en ;
    dcat:contactPoint persons:yhchae ; # vcard:Kind - https://www.w3.org/TR/vcard-rdf/#Kind Examples
    dcat:hasCurrentVersion knpp:opr1000-nodes-image-2023-10-27 ; # pav:hasCurrentVersion
    dact:first  knpp:opr1000-nodes-image-2023-10-27 ;
    dcat:hasVersion knpp:opr1000-nodes-image-2023-10-27 ;
    dcat:keyword    "opr1000"@en ;
    dcat:keyword    "nordalization"@en ;
    dcat:keyword    "image"@en ;
    dcat:landingPage    <http://kaeri.re.kr/npp/opr1000/opr1000-nodes-image-2023-10-27.png> ;    # foaf:page
    dcat:last   knpp:opr1000-nodes-image-2023-10-27 ;
    dcat:prev   "" ;    # or not defined, because it's the first version
    dcat:qualifiedRelation "" ;  # 15.2 in DCAT v3, 련재까지 선언하지 않았음
    dcat:theme "" ; # 5.4 & 6.3.2 in DCAT V3, dcat:themeTaxonomy, npp에 대한 Taxonomy, Thesaurus 또는 Ontology 구축 후 fill 해야 할 듯
    dcat:version    "1.0" ;   # version number
    dcterms:accessRights <http://publications.europa.eu/resource/authority/access-right/PUBLIC> ; # dcsterms:accessRights
    dcterms:confirmsTo <http://www.kaeri.re.kr/dcat-ap-npp> ; # dcat-ap-npp를 만들 필요가 있음. 
    dcterms:creator person:yhchae ; # foaf:Agent
    dcterms:description "opr 1000 모델 원자로을 채택한 원자력 발전소의 nordalization 그림"@ko ;  #
    dcterms:hasPart ""  # 현재는 이 resource에 대한 구성요소 없음
    dcterms:identifier  "resource-001"    # 현재는 임의로 지정하나 시스템에서는 생성된 identifier를 사용하여야 함
    dcterms:isRefernecedBy  ""  # 선택적 속성 같음. 어떤 다른 resource가 참조하는 것을 나타냄. 예를 들어 이 그림을 한 문서에서 사용하면 그 문서를 가림킴, 그러나 푠준 문서에서는 domain과 range가 불분명함
    dcterms:issued "2023-10-25"^^xsd:date ;  #
    dcterms:language <http://id.loc.gov/vocabulary/iso639-1/ko> ;   # Korean
    dcterms:language <http://id.loc.gov/vocabulary/iso639-1/en> ;   # English
    dcterms:license <https://creativecommons.org/licenses/by/4.0/> ;    #
    dcterms:modified "" ;   # 현재까지는 아직 변경 사항 없음, 그러나 발생하면 위의 dcterms:issued 속성 형식으로 작성하여야 함
    dcterms:publisher organization:kaeri ;  # 발행 기관을 원자력연구원으로 함
    dcterms:relation  "" ; # 사용법이 아직 불분영하므로 skip
    dcterms:replaces "" # see more in "https://www.w3.org/TR/vocab-dcat-3/#Property:resource_relation"
    dcterms:rights [ a dcterms:RightsStatement ;
        rdfs:label "2023 KAERI"@en
    ] ;   # 아직 속성 accessRights와 license 차별성이 모호함
    dcterms:title   "opr 1000 모델 원자로을 채택한 원자력 발전소의 nordalization 그림"@ko  #
    dcterms:type <http://purl.org/dc/dcmitype/Image> ; # image 타입임을 선언
    ordl:hasPolicy "" ; # see more in "https://www.w3.org/TR/vocab-dcat-3/#ex-odrl-policy" not specified here, but in Distribution
    prov:qualifiedAttribution "" ;  # see more in "https://www.w3.org/TR/vocab-dcat-3/#qualified-attribution" not specified here
    .

# csv type 파일의 한 example, 그러나 master table과 비슷한 역할을 수행
# 이같은 image 파일은 Catalog Distribution, dataset-001에 한 instance로 등록되어야 할 것 같음
# "A-TH-15-6-5-SBLOCA-CL-CHA-0.9INCH.csv" 파일의 "Node Desc" Sheet
# https://www.iana.org/assignments/media-types/media-types.xhtml#text for the reference "Node Desc" sheet
knpp:opr1000-node-description   # resource-opr1000-node-description
    a dcat:Resource ;
    adms:status <http://purl.org/adms/status/Completed> ;   # status is one of “completed”, “deprecated”, “under development”, and “withdrawn” from http://purl.org/adms/status/
    adms:versionNotes “the first version of node description for NPP with OPR 1000 nuclear reactor type”@en ;
    dcat:contactPoint persons:yhchae ; # vcard:Kind - https://www.w3.org/TR/vcard-rdf/#Kind Examples
    dcat:hasCurrentVersion knpp:opr1000-nodes-image-2023-10-29 ; # pav:hasCurrentVersion
    dact:first  knpp:opr1000-node-description-2023-10-29 ;
    dcat:hasVersion knpp:opr1000-node-description-2023-10-29 ;
    dcat:keyword    "opr1000"@en ;
    dcat:keyword    "node description"@en ;
    dcat:landingPage    <http://kaeri.re.kr/npp/opr1000/opr1000-node-description-2023-10-29.csv> ;    # foaf:page
    dcat:last   knpp:opr1000-nodes-image-2023-10-29 ;
    dcat:prev   "" ;    # or not defined, because it's the first version
    dcat:qualifiedRelation "" ;  # 15.2 in DCAT v3, 련재까지 선언하지 않았음
    dcat:theme "opr1000" ; # 5.4 & 6.3.2 in DCAT V3, dcat:themeTaxonomy, npp에 대한 Taxonomy, Thesaurus 또는 Ontology 구축 후 fill 해야 할 듯
    dcat:theme "node" ;
    dcat:version    "1.0" ;   # version number
    dcterms:accessRights <http://publications.europa.eu/resource/authority/access-right/PUBLIC> ; # dcsterms:accessRights
    dcterms:confirmsTo <http://www.kaeri.re.kr/dcat-ap-npp> ; # dcat-ap-npp를 만들 필요가 있음. 
    dcterms:creator person:yhchae ; # foaf:Agent
    dcterms:description "opr 1000 모델 원자로을 채택한 원자력 발전소의 node description이 저장된 csv 파일"@ko ;  #
    dcterms:hasPart ""  # 현재는 이 resource에 대한 구성요소 있을 것 같음(?)
    dcterms:identifier  "resource-002"    # 현재는 임의로 지정하나 시스템에서는 생성된 identifier를 사용하여야 함
    dcterms:isRefernecedBy  "resource-003"  # P sheet in "A-TH-15-6-5-SBLOCA-CL-CHA-0.9INCH.csv" 파일
    dcterms:issued "opr1000-node-description-2023-10-29"^^xsd:date ;  #
    dcterms:language <http://id.loc.gov/vocabulary/iso639-1/ko> ;   # Korean
    dcterms:language <http://id.loc.gov/vocabulary/iso639-1/en> ;   # English
    dcterms:license <https://creativecommons.org/licenses/by/4.0/> ;    #
    dcterms:modified "" ;   # 현재까지는 아직 변경 사항 없음, 그러나 발생하면 위의 dcterms:issued 속성 형식으로 작성하여야 함
    dcterms:publisher organization:kaeri ;  # 발행 기관을 원자력연구원으로 함
    dcterms:relation  "" ; # 사용법이 아직 불분영하므로 skip
    dcterms:replaces "" # see more in "https://www.w3.org/TR/vocab-dcat-3/#Property:resource_relation"
    dcterms:rights [ a dcterms:RightsStatement ;
        rdfs:label "2023 KAERI"@en
    ] ;   # 아직 속성 accessRights와 license 차별성이 모호함
    dcterms:title   "opr 1000 모델 원자로을 채택한 원자력 발전소의 node description"@ko  #
    dcterms:type <http://www.iana.org/assignments/media-types/text/csv> ; # csv 타입임을 선언 or/and 
    dcterms:type <http://www.iana.org/assignments/media-types/text/csv-schema> ; # 다른 csv파일의 schema임을 선언
    ordl:hasPolicy "" ; # see more in "https://www.w3.org/TR/vocab-dcat-3/#ex-odrl-policy" not specified here, but in Distribution
    prov:qualifiedAttribution "" ;  # see more in "https://www.w3.org/TR/vocab-dcat-3/#qualified-attribution" not specified here
    .

# for more about metadata of csv file: https://www.gov.uk/government/publications/recommended-open-standards-for-government/using-metadata-to-describe-csv-data

